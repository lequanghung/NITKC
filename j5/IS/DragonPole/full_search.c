#include <stdio.h>
#include "dragonPole.h"
#include <math.h>
#include <time.h>

void dragonpoleMain();
int calcBattlePoint(int *selection);

int main(void)
{
  int i, j, d, start, end;
  int point, max_point;
  int selection[ITEM];
  
  /* システム初期化 */
  dragonpoleMain();
  start = clock();
  max_point =0;
  
  /* int から binへの変換 */
  for (i=0; i<pow(2,ITEM); i++){
    for (j=ITEM-1; j>=0; j--){

      d = i >> j;

      if (d & 1)
	selection[j] = 1;
      else
	selection[j] = 0;
      printf("%d",selection[j]);
    }

    /* 戦力計算 */
    point = calcBattlePoint(selection);
    if(point > max_point)
      max_point = point;
    printf("  Power: %d\n", point);
  }
  
  end = clock();
  printf("Processing time: %.d [ms] = %.3f [s] = %.3f [min]\n",
	 end-start,
	 (float )(end-start)/1000,
	 (float )(end-start)/1000/60);
  printf("Max power: %d\n", max_point);
  
  return 0;
}       
