#include <stdio.h>
#include <stdlib.h>
#include "dragonPole.h"
#include <math.h>
#include <time.h>

void dragonpoleMain();
int calcBattlePoint(int *selection);
void show_val(int *val){
  int i;
  for(i=0; i<ITEM; i++){
    printf("%d",val[i]);
  }
}

int main(void)
{
  int i, j, k, start, end;
  int max_p, tmp_p, loc;
  int tmp[ITEM];
  int max[ITEM];
    
  /* システム初期化 */
  dragonpoleMain();
  srand((int)time(NULL));
  start = clock();

  /* 初回のみ全ビットランダム */
  for (i=0; i<ITEM; i++){
    max[i] = 0;
    tmp[i] = max[i];
  }
  for(i=0; i<BIT; i++){
    j = rand()%ITEM;
    max[j] =1;
    tmp[j] =1;
  }
  printf("Start : \n");
  show_val(max);
  puts("");
  
  /* TIMES回だけ1ビットランダム */
  while(loc != -1){
    loc = 0;
    for(k=0; k<ITEM; k++){
      tmp[k] = 1 - tmp[k];
      tmp_p =calcBattlePoint(tmp);
      
      /* 戦力計算 */
      if (tmp_p > max_p){
	max_p = tmp_p;
	loc = k;
      }
      tmp[k] = 1 - tmp[k];
    }
    
    if (loc!= 0){
      max[loc] = 1 - max[loc];
      tmp[loc] = max[loc];
    }else loc = -1;
  }
  end = clock();
  printf("Random searched.\n");
  printf("Processing time: %.d [ns] = %.3f [s] = %.3f [min]\n",
	 (end-start),
	 (float )(end-start)/CLOCKS_PER_SEC,
	 (float )(end-start)/CLOCKS_PER_SEC/60);
  show_val(max);
  printf(" Max power: %d\n", max_p);
  
  return 0;
}       
