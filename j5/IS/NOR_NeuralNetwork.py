import math

def fx(x):
    ESP = 4.0
    return 1/(1+math.exp(-ESP*x))

offa = -1.312300
offb = 1.956707
offc = 0.710779
wba = 2.743674
wca = -2.778689
wdb = -1.310456
web = -1.312891
wdc = -1.691827
wec = -1.704581

input = [[0,0,0],[0,1,1],[1,0,1],[1,1,0]]

for i in input:
    d = i[0]
    e = i[1]
    b = fx(d*wdb+e*web+offb)
    c = fx(d*wdc+e*wec+offc)
    a = fx(b*wba+c*wca+offa)
    print i[2],a
