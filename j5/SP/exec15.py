''''''''''''''
課題１５：DFTとIDFTのプログラムを作成する
''''''''''''''
import numpy as np

def DFT(xn):
    N = len(xn)
    mtx = np.zeros((N,N), dtype = np.complex_)
    for i in range(N):
        for j in range(N):
            real =  np.cos(2*np.pi*i*j/N)
            imag = -np.sin(2*np.pi*i*j/N)
            if abs(real) < 1e-6:
                real = 0.0
            if abs(imag) < 1e-7:
                imag = 0.0
            mtx[i][j] = complex(real,imag)
    print()
    print(mtx)
    print()
    print(mtx@xn)

def IDFT(Xk):
    N = len(Xk)
    mtx = np.zeros((N,N), dtype = np.complex_)
    for i in range(N):
        for j in range(N):
            real =  np.cos(-2*np.pi*i*j/N)
            imag = -np.sin(-2*np.pi*i*j/N)
            if abs(real) < 1e-6:
                real = 0.0
            if abs(imag) < 1e-7:
                imag = 0.0
            mtx[i][j] = complex(real,imag)
    print()
    print(mtx)
    print()
    print(1/N*mtx@Xk)
        
xn = np.array([2,-2,-3,7])

Xk = np.array([4,5+9j,-6,5-9j])

print(xn)
DFT(xn)
IDFT(Xk)
