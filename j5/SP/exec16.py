'''''''''''''''''''''''''''''''''''''''''''''''''''
課題１６：長さ１０〜２００の２０種類のx(n)を作成し、
DFT変換時間(Tc)を求める。Tcと長さの関係をフラフ化する
'''''''''''''''''''''''''''''''''''''''''''''''''''

import numpy  as np
import random as rd
import time   as ti
import matplotlib.pyplot as plt

def DFT(xn):
    N = xn.shape[0]
    mtx = np.zeros((N,N), dtype = np.complex_)
    for i in range(N):
        for j in range(N):
            real =  np.cos(2*np.pi*i*j/N)
            imag = -np.sin(2*np.pi*i*j/N)
            if abs(real) < 1e-6:
                real = 0.0
            if abs(imag) < 1e-7:
                imag = 0.0
            mtx[i][j] = complex(real,imag)
    xn = np.matmul(mtx,xn)
 
len = []
time = []

for i in range(20):
    k = rd.randint(10,200)
    len.append(k)
    xn = np.random.randint(10,size = k)
    start = ti.time()
    DFT(xn)
    time.append(ti.time() - start)

print(len)
plt.plot(len,time,"o")
plt.show()
