# code : utf-8
import numpy as np
import matplotlib.pyplot as plt

N = 128
t = np.arange(0,N,1)
#carrier freq
fc = 4

print ("input bits array = ")
str=input()
signal=[int(i) for i in str]
#ASK (Amplitude Shift Keying) signal
ask = []
#FSK (Frequency Shift keying) signal
fsk =[]
#PSK (Phase Shift Keying) signal
psk = []

wave0=np.sin(2*np.pi*t/N*fc)*0
wave1=np.sin(2*np.pi*t/N*fc)
wave2=np.sin(2*np.pi*t/N*fc*0.5)
wave3=np.sin(2*np.pi*t/N*fc+np.pi)

for i in signal:
    if i==0:                #data = 0
        ask.extend(wave0)   #add new list to end of list
        fsk.extend(wave2)
        psk.extend(wave1)
    else:
        ask.extend(wave1)
        fsk.extend(wave1)
        psk.extend(wave3)

xs=np.repeat(range(len(signal)),2)
ys=np.repeat(signal,2)
xs = xs[1:]
ys = ys[:-1]

plt.subplots_adjust(hspace=0.5)
plt.subplot(221)
plt.plot(xs,ys)
plt.title("Digital signal")
plt.subplot(222)
plt.plot(range(len(ask)),ask) #len(ask) for length of ASK list
plt.title("ASK signal")
plt.subplot(223)
plt.plot(range(len(fsk)),fsk)
plt.title("FSK signal")
plt.subplot(224)
plt.plot(range(len(psk)),psk)
plt.title("PSK signal")
plt.show()
