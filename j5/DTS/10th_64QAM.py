'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
 *******64-Quadrature Amplitude Modulation(QAM)**********
                             ^ y                            
                             |
                             |
                 -7 -5 -3 -1 | 1  3  5  7
              7   +  +  +  + | +  +  +  +
              5   +  +  +  + | +  +  +  +
              3   +  +  +  + | +  +  +  +
              1   +  +  +  + | +  +  +  +   x
                ---------------------------->
             -1   +  +  +  + | +  +  +  +
             -3   +  +  +  + | +  +  +  +
             -5   +  +  +  + | +  +  +  +
             -7   +  +  +  + | +  +  +  +
                             |

　一度に64(2^6)状態、つまり６ビットごとを転送する
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
import numpy as np
import random as rd
import textwrap as tw
import matplotlib.pyplot as plt

def wave(x,y):
    A=x
    if x>0:                   
        r=np.arctan(x/y)
    else:
        r=np.arctan(x/y)+np.pi/2
        
    return A*np.sin((2*np.pi*t/N*fc)+r)

#raw signal sampling 
N = 128
t = np.arange(0,N,1)
fc = 2


#genarate 60 random bits
sig = []
while len(sig)<60:
    sig.append(rd.randint(0,1))
tmp = []

#2^6 =64
bit_len = 6

sig = "".join(str(i) for i in sig)  #list to string
sig = tw.wrap(sig,bit_len)          #split string
print(sig)
for i in range(len(sig)):
    sig[i] = int(sig[i],2)          #replace string by int
    x = 7-sig[i]//8*2               #covert to axis
    y = 7-sig[i]%8*2
    tmp.extend(wave(x,y))
  
#Plot 64QAM signal
plt.plot(range(len(tmp)),tmp)
plt.title("64QAM signal")
plt.xlim(0,len(sig)*N)
plt.xticks(list(map(lambda x: x*N, np.arange(0,len(sig)+1))),[''])
plt.ylim(-7,7)
plt.yticks(list(range(-7,8)))
plt.grid(linestyle='--')
plt.show()

