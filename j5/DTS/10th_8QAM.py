'''
 *******8-Quadrature Amplitude Modulation(QAM)**********
                             A:振幅, r:位相  
       +++++++011+++++++     000: A=0.5, r=0
       +   +++010+++   +     001: A=1  , r=0
       +   +   |   +   +     010: A=0.5, r=pi/2
      101 100-----000 001    011: A=1  , r=pi/2
       +   +   |   +   +     100: A=0.5, r=pi
       +   +++110+++   +     101: A=1  , r=pi
       +++++++111+++++++     110: A=0.5, r=3*pi/2
                             111: A=1  , r=3*pi/2
'''
import numpy as np
import matplotlib.pyplot as plt

def wave(A,x):
    return A*np.sin((2*np.pi*t/N*fc)+x*np.pi)

N = 128
t = np.arange(0,N,1)
fc = 2

#[001,100,000,011,101,110]
sig = [0,0,1,1,0,0,0,0,0,0,1,1,1,0,1,1,1,0]
tmp = []
sig = np.array(sig).reshape((int)(len(sig)/3),3)

for i in sig:
    if (i == [0,0,0]).all():
        tmp.extend(wave(0.5,0))
    elif (i == [0,0,1]).all():
        tmp.extend(wave(1,0))
    elif (i == [0,1,0]).all():
        tmp.extend(wave(0.5,0.5))
    elif (i == [0,1,1]).all():
        tmp.extend(wave(1,0.5))
    elif (i == [1,0,0]).all():
        tmp.extend(wave(0.5,1))
    elif (i == [1,0,1]).all():
        tmp.extend(wave(1,1))
    elif (i == [1,1,0]).all():
        tmp.extend((0.5,1.5))
    elif (i == [1,1,1]).all():
        tmp.extend(wave(1,1.5))
        
#Plot 8QAM signal
plt.plot(range(len(tmp)),tmp)
plt.title("8QAM signal")
plt.xticks(list(map(lambda x: x*N, np.arange(0,len(sig)))),[''])
plt.ylim(-1.2,1.2)
plt.yticks(list(map(lambda y: y*0.5, np.arange(-2,3))))
plt.grid(linestyle='--')
plt.show()
