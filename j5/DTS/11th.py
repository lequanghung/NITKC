# -*-coding:utf-8-*-
import math
import numpy as np
import matplotlib.pyplot as plt

#Transmission loss
def trans_loss(f):
    return 1.967*np.sqrt(f) + 0.023*f + 0.05/np.sqrt(f)

#Near-End-Crosstalk
def NEXT(f):
    return 35.3-15*np.log10(f/100)

#Far-End-Crosstalk
def FEXT(f):
    return 23.8-20*np.log10(f/100)

#Creat the table
A = np.zeros([3,8])
A = np.insert(A,0,[1, 4, 10, 20, 25, 31.25, 62.5, 100],axis=0)
for j in range(A.shape[1]):
    A[1][j] = trans_loss(A[0][j])
    A[2][j] = NEXT(A[0][j])
    A[3][j] = FEXT(A[0][j])

print(A)

f = np.arange(1,100,0.5)
loss_data = []
NEXT_data = []
FEXT_data = []
for i in f:
    loss_data.append(trans_loss(i))
    NEXT_data.append(NEXT(i))
    FEXT_data.append(FEXT(i))
    
plt.subplot(131)
plt.plot(f,loss_data)
plt.title("Transmission loss")

plt.subplot(132)
plt.plot(f,NEXT_data)
plt.title("Near-End-Crosstalk")

plt.subplot(133)
plt.plot(f,FEXT_data)
plt.title("Far-End-Crosstalk")

plt.show()
