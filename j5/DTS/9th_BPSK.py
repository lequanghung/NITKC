import numpy as np
import matplotlib.pyplot as plt

N = 128
t = np.arange(0,N,1)
fc = 2
#[1,0,0,0,1,1,1,0]
signal = [1,-1,-1,-1,1,1,1,-1]
BPSK= []
baseband_signal=[]

wave0 = np.cos(2*np.pi*t/N*fc)
wave1 = np.cos((2*np.pi*t/N*fc)+np.pi)

for i in signal:
    if i==1:
        BPSK.extend(wave0)
        baseband_signal.extend([1]*N)
    else:
        BPSK.extend(wave1)
        baseband_signal.extend([-1]*N)

#Plot baseband signal
plt.subplot(221)
plt.ylim(-1.5,1.5)
plt.plot(range(len(baseband_signal)),baseband_signal)
plt.title("Baseband signal")

#Plot modulated signal
plt.subplot(222)
plt.ylim(-1.5,1.5)
plt.plot(range(len(BPSK)),BPSK)
plt.title("Binary PSK signal");

#Creat signal for demodulation 
sig4de=[]
for i in signal:
    sig4de.extend(np.cos(2*np.pi*t/N*fc))

#Multiply BPSK signal with sig4de
BPSK=np.array(BPSK)
sig4de=np.array(sig4de)
BPSK*=sig4de

#Plot received signal
plt.subplot(223)
plt.ylim(-1.5,1.5)
plt.plot(range(len(BPSK)),BPSK)
plt.title("Receive signal");

#Fast Fourier Transform 
BPSK=np.fft.fftshift(BPSK)
BPSK=np.fft.fft(BPSK)
BPSK=np.fft.fftshift(BPSK)
BPSK[:512-16]=0
BPSK[512+16:]=0
BPSK=np.fft.fftshift(BPSK)
BPSK=np.fft.ifft(BPSK)
BPSK=np.fft.fftshift(BPSK)

#Plot demodulated signal
plt.subplot(224)
plt.ylim(-1.5,1.5)
plt.plot(range(len(BPSK)),BPSK)
plt.title("Demodulated signal");

plt.show()
