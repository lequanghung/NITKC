# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
def st(f):
    return np.cos(2*np.pi*t/N*f)

N = 128  #sampling freq
f0 = 32  #carrier freq
f1 = 4   #transmit freq
A = 1
a = 1

t = np.arange(0, N)
ft = np.cos(2*np.pi*t/N*f0)  #carrier signal
am = A*(a*st(f1)+1)*ft       # 4Hz
#8Hz+7Hz+6Hz+5Hz+4Hz
am2 = A*(a*st(4)+1)*ft + A*(a*st(5)+1)*ft+A*(a*st(6)+1)*ft+A*(a*st(7)+1)*ft+A*(a*st(8)+1)*ft

plt.subplot(221)
plt.plot(t,am)
F = np.fft.fft(am)
F = np.abs(F)**2
plt.subplot(222)
plt.plot(t,F)

plt.subplot(223)
plt.plot(t,am2)
F = np.fft.fft(am2)
F = np.abs(F)**2
plt.subplot(224)
plt.plot(t,F)
plt.show()
