# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

#define length of bits
def LENGTH():
    return 1024

#Non-return-to-zero level
def NRZL (data):
    #[0 0 1 1 ... len len]
    xs = np.repeat(range(len(bits)), 2)
    #double bit array
    ys = np.repeat(bits, 2)
    #inverted bit array
    ys = -ys
    xs = xs[1:]
    ys = ys[:-1]

    plt.subplot(4,1,1)
    plt.ylim(-1.5,0.5)
    plt.plot(xs,ys)
    plt.title("Non-return-to-zero level")
    plt.xticks([])
    plt.yticks([-1,0],('-E','0'))

#Non-return-to-zero inverted
def NRZI(data):
    xs = np.repeat(range(len(bits)), 2)
    ys = np.repeat(bits, 2)
    xs = xs[1:]
    ys = ys[:-1]
    #change bit val : 0->1,1->-1
    for i in range(len(ys)):
        if (ys[i] == 0):
            ys[i] = 1
        else :
            ys[i] = -1

    plt.subplot(4,1,2)
    plt.ylim(-1.5,1.5)
    plt.plot(xs,ys)
    plt.title("Non-return-to-zero inverted")
    plt.xticks([])
    plt.yticks([-1,0,1],('-E','0','E'))

#Return-to-zero level
def RZL (data):
    #[0 0 1 1 ...  2*len 2*len]
    xs = np.repeat(range(len(bits)*2),2)
    ys = bits
    #insert 0 after each bit
    ys = np.insert(ys,range(len(bits)),0)
    #double bit array
    ys = np.repeat(ys,2)
    ys = -ys
    xs = xs[1:]
    ys = ys[:-1]

    plt.subplot(4,1,3)
    plt.ylim(-1.5,0.5)
    plt.plot(xs,ys)
    plt.title("Return-to-zero level")
    plt.xticks([])
    plt.yticks([-1,0],('-E','0'))

#Return-to-zero inverted
def RZI(data):
    xs = np.repeat(range(len(bits)*2),2)
    ys = np.array(bits)
    #change bit val : 0->1,1->-1
    for i in range(len(ys)):
        if (ys[i] == 0):
            ys[i] = 1
        else :
            ys[i] = -1
    #insert 0 after each bit
    ys = np.insert(ys,range(len(bits)),0)
    ys = np.repeat(ys,2)
    xs = xs[1:]
    ys = ys[:-1]

    plt.subplot(4,1,4)
    plt.ylim(-1.5,1.5)
    plt.plot(xs,ys)
    plt.title("Return-to-zero inverted")
    plt.xticks([])
    plt.yticks([-1,0,1],('-E','0','E'))

bits = np.random.randint(0,2,LENGTH())
#print (bits)
plt.subplots_adjust(wspace=0, hspace=0.5)
NRZL(bits)
NRZI(bits)
RZL(bits)
RZI(bits)
plt.show()
