#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

#Manchester encode
# 0 -> 01
# 1 -> 10
def Manchester (bits):    
    bits = np.array([int(i) for i in bits])
    xs = np.repeat(range(len(bits)), 2)
    ys = np.repeat(bits, 2)
    print("RAW*2:")
    print(ys)
    for i in range(len(ys)):
        if (i%2 == 1):
            ys[i] = 1 - ys[i]
    print ("Manchester:")
    print (ys)

    plt.plot(xs, ys)
    

# MLT-3 encode
# 0 -> copy the previous bit
# 1 -> change in order 0->1->0->-1->0
def MLT3(bits):
    MLT3 =[]
    cnt = 0
    bits = endcoded_4B5B(bits)
    for i in range(len(bits)):
        if bits[i] == '0':
            if i == 0:
                MLT3.append(0)
            else:
                MLT3.append(MLT3[i-1])
        else:
            cnt += 1
            if (cnt%4) == 1:
                MLT3.append(1)
            elif (cnt%4) == 3:
                MLT3.append(-1)
            else:
                MLT3.append(0)

    print("MLT3:")
    ys = np.array(MLT3, dtype=np.int)
    print (ys)

# 4B5B encode (4 bits array -> 5 bits array)    
def endcoded_4B5B(bits):
    bits =  [bits[i:i+4] for i in range(0, len(bits), 4)]
    print ("4B:")
    print (bits)
    result = []
    encode_5B = ['11110', '01001', '10100', '10101', '01010',
                 '01011', '01110', '01111', '10010', '10011',
                 '10110', '10111', '11010', '11011', '11100', '11101']
    for i in bits:
        result.append(encode_5B[int(i, 2)])
    print ("5B:")
    print (result)    
    return ''.join(result)

# Display raw signal
def Raw(bits):
    bits = np.array([int(i) for i in bits])
    print ("RAW:")
    print (bits)
    
    plt.plot(np.arange(len(bits)), bits)

if __name__ == '__main__':
    bits = '010000111111'
    Raw(bits)
    Manchester(bits)
    MLT3(bits)
    plt.show()
