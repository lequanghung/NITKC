#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import random

N = 51200
x = np.arange(0, N)
sig0 = np.zeros(N)
sig1 = np.zeros(N)
aver0 = 0
aver1 = 0

for i in range(N):
    sig0[i] = random.uniform(-2, 0)
    sig1[i] = random.uniform(-2, 0)
    aver0 += sig0[i]
    aver1 += sig0[i] + sig1[i]
    
print ("直流成分: {0}\n衝突状態の直流成分: {1}".format(aver0/N, aver1/N))
##  最小フレーム: 64bytes = 64x8 = 512 bits 
##  送出時間: v_{g} = 512/転送速度
##  遅延t_{g} = 2*SUM(L)/v_{g} + 2*SUM(t)
print ("最大許容ケーブル長:")
print ("転送速度: 100Mbps, ケーブル: 1 本\n  ---> {0:.3f}[m]".format(1/2*512/(100*10**6)*2.3*10**8))
print ("転送速度: 1Gbps, ケーブル: 1 本\n  ---> {0:.3f}[m]".format(1/2*512/(100*10**9)*2.3*10**8))
print ("転送速度: 100Mbps, 時間遅延 1.0 us のリピータ: 1 台,  ケーブル: 2 本\n ---> {0:.3f}[m]".format(1/2/2*(51.2-2*1)*10**-6*2.3*10**8))
print ("転送速度: 100Mbps, 時間遅延 1.0 us のリピータ: 2 台,  ケーブル: 3 本\n ---> {0:.3f}[m]".format(1/2/3*(51.2-4*1)*10**-6*2.3*10**8))
