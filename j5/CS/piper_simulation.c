#include <stdio.h>

#define MEM_LEN 100
#define REG_LEN 16

// global variables for memories(MEM), regiters(Reg), instruction(Ints)
signed int Mem[100] = {0};
signed int Reg[16] = {0};
int temp_reg[4] = {0};


void show_Reg(){
  int i;
  for (i = 0; i<REG_LEN; i++){
    printf("Reg[%2d]=%3d, ",i , Reg[i]);
    if (i%4 == 3)
      printf("\n");
  }
}

void show_temp(){
  int i;
  for (i=0; i<4; i++){
    printf("%d ", temp_reg[i]);
  }
  printf("\n");
}

void IF(int i){
  temp_reg[3] = Mem[i];
}

void ID(){
  // op code
  temp_reg[0] = (temp_reg[3] >>12 & 0x0f);
  // 1st op
  temp_reg[1] = (temp_reg[3] >> 8) & 0x0f;
  
  if (temp_reg[0] == 0 ){
    printf ("No op code\n");
  } else if (temp_reg[0] <= 2){   // Load/Store Instructions
    // 2nd op
    temp_reg[2] = (temp_reg[3]) & 0xff;
  } else {                    // others Instructions
    // 2nd op
    temp_reg[2] = (temp_reg[3] >> 4) & 0x0f;
    // 3rd op
    temp_reg[3] = (temp_reg[3]) & 0x0f;
  }
}

void EX(){
  switch(temp_reg[0]){
  case 1 : temp_reg[3] = 0; break;
  case 2 : temp_reg[3] = 0; break;
  case 3 : temp_reg[2] = Reg[temp_reg[2]] + Reg[temp_reg[3]]; break;
  case 4 : temp_reg[2] = Reg[temp_reg[2]] - Reg[temp_reg[3]]; break;
  case 5 : temp_reg[2] = Reg[temp_reg[2]] * Reg[temp_reg[3]]; break;
  case 6 : temp_reg[2] = Reg[temp_reg[2]] / Reg[temp_reg[3]]; break;
  default : break;
  }
}

void MEM(){
  switch(temp_reg[0]){
  case 1 : temp_reg[2] = Mem[temp_reg[2]]; break;
  case 2 : Mem[temp_reg[2]] = Reg[temp_reg[1]]; break;
  default : break;
  }
}

void WB(){
  switch(temp_reg[0]){
  case 1 : Reg[temp_reg[1]] = temp_reg[2]; break;
  case 2 : break;
  default : Reg[temp_reg[1]] = temp_reg[2]; break;
  }
}

int main (void){
  
  Mem[0] = 14593;
  Mem[1] = 23186;
  Mem[2] = 27555;
  Mem[3] = 19636;
  
  Reg[0] = 17;
  Reg[1] = 1;
  Reg[2] = 3;
  Reg[3] = 2;
  Reg[4] = 23;
  
  show_Reg();
  int i;
  for ( i=0; i<4; i++){
    printf("[%d]:\n",i);
    IF(i);
    ID();
    show_temp();
    EX();
    MEM();
    WB();
    show_Reg();
  }
  return 0;
}
