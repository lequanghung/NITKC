#include <stdio.h>

class Add{
  float a,b,sum;
public:
  void set_a(float _a);
  void set_b(float _b);
  float get_sum(void);
  void calc(void);
};

void Add::set_a(float _a){
  a = _a;
}

void Add::set_b(float _b){
  b = _b;
}

void Add::calc(void){
  sum = a + b;
}
  
float Add::get_sum(void){
  return sum;
}

int main(void){
  Add p;
  p.set_a(1000);
  p.set_b(2000);
  p.calc();
  printf("sum = %f\n", p.get_sum());

  return 0;
}
