#include <iostream>
using namespace std;

class D_Latch{
  int w[6] = {0,0,1,1,0,1};

public:
  int exec(int _d, int _strobe);
};

int D_Latch::exec(int _d, int _strobe){
  w[0] = _d;
  w[1] = _strobe;

  //計算
  w[2] = !(w[0] && w[1]);
  w[3] = !(w[2] && w[1]);
  w[4] = !(w[2] && w[5]);
  w[5] = !(w[4] && w[3]);

  return w[4];
}

int main(void){

  int d = 0;
  int stb = 0;
  int s;
  D_Latch p;

  cout << "d = " << d <<", stb = " << stb << endl;
  cout << p.exec(d, stb) << endl;
  cout << p.exec(d, stb) << endl;
  cout << p.exec(d, stb) << endl;
  
  d = 1;
  cout << "d = " << d <<", stb = " << stb << endl;
  cout << p.exec(d, stb) << endl;
  cout << p.exec(d, stb) << endl;
  cout << p.exec(d, stb) << endl;

  d = 0; stb = 1;
  cout << "d = " << d <<", stb = " << stb << endl;
  cout << p.exec(d, stb) << endl;
  cout << p.exec(d, stb) << endl;
  cout << p.exec(d, stb) << endl;

  d = 1;
  cout << "d = " << d <<", stb = " << stb << endl;
  cout << p.exec(d, stb) << endl;
  cout << p.exec(d, stb) << endl;
  cout << p.exec(d, stb) << endl;

  return 0;
}
