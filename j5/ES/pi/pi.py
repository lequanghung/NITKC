#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import random as rd
import sys, time

class loop():
    def __init__(self):
        self.argv = sys.argv
        self.argc = len(sys.argv)
        if (self.argc != 2):
            print ("!!! Usage: python {0} [LOOPTIME]".format(self.argv[0]))
    def get_input(self):
        return int(self.argv[1])                
#    Monte Carlo(モンテカルロ) method:
# 1. random genarate (x,y) in [0,1]
# 2. if x^2+y^2 <= 1 then count+1 else do nothing
# 3. loop for N times
# 4. pi = count*4/N (cos 0<=(x, y)<= 1 is only a quad of circle
def MonteCarlo(N):
    start = time.perf_counter()
    count = 0
    for i in range(N):
        x = rd.random()
        y = rd.random()
        if (x*x + y*y <= 1):
            count += 1
    print ("MonteCarlo method:")
    print("{:1.20f} TIME: {}[ms]".format(count*4/N, time.perf_counter()-start))

# Leibniz(ライプニッツ) method:
# pi/4 = 1 - 1/3 + 1/5 - 1/7 + 1/9 - 1/11 + …
def Leibniz(N):
    start = time.perf_counter()
    pi = 0
    for i in range(N):
        pi += (1 / (i * 4 + 1) - 1 / (i * 4 + 3))
    print ("Leibniz method:")
    print("{:1.20f} TIME: {}[ms]".format(pi * 4, time.perf_counter()-start))

# GaussLegendre (ガウス・ルジャンドル) method:
# a = 1, b = 1/sqrt(2), t = 1/4, q = 1
# a' = (a + b)/2, b' = sqrt(ab), t' = t - p(a-a')^2, p' = 2p
def update(a, b, t, p):
    new_a = (a+b)/2.0
    new_b = np.sqrt(a*b)
    new_t = t-p*(a-new_a)**2
    new_p = 2*p
    return new_a,new_b,new_t,new_p

def GaussLegendre(N):
    start = time.perf_counter()
    a = np.float(1.0); b = 1/np.sqrt(2)
    t = 0.25; p = 1.0
    print ((a+b)**2/4/t)
    for i in range(N):
        a,b,t,p = update(a,b,t,p)
    print("GaussLegendre method:")
    print ("{0:1.20f} TIME: {1}[ms]".format((a+b)**2/(4*t), time.perf_counter() -start))
        
if __name__ == '__main__':
    N = loop().get_input()
    MonteCarlo(N)
    Leibniz(N)
    GaussLegendre(N)
