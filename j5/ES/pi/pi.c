#include <stdio.h>
#include <math.h>
#include "MT.h"

#define LOOP 1e5

int main(int agrc, char agrv[]){
  
  int i;
  int seed = 0;
  int count = 0;
  
  init_genrand(seed);
  
  for (i=0; i<LOOP; i++){
    if (hypot(genrand_real1(), genrand_real1()) <= 1){
      count++;
    }
  }

  printf("LOOP TIMES: %.0e\n", LOOP);
  printf("pi = %lf\n", count*4/LOOP);

  return 0;
}
  
	
