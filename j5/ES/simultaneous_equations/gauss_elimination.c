#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//ファイルから係数を入力
void  inputData(int N, char filename[256], double A[N][N+1])
{
  FILE *fp;
  int i,j;

  fp = fopen(filename,"r");
  if(fp==NULL){
    printf("Error.\n");
    printf("Cannot open file.\n");
    exit(1);
  }

  for(i=0; i<N; i++){
    for(j=0; j<N+1; j++){
      fscanf(fp, "%lf", &A[i][j]);
    }
  }
  
  fclose(fp);
}
  
//行列Aの要素を表示
void print(int N, double A[N][N+1]){

  int i,j;

  for (i=0; i<N; i++){
    for(j=0; j<N+1; j++){
      printf("%10.2lf",A[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//Gauss elimination (ガウス消去法)
void ge(int N, double A[N][N+1]){

  int i,j,k;
  double pivot,del;

  printf("forward elimination:\n");
  for(k=0; k<N; k++){
    pivot = A[k][k];
    for(j=k; j<N+1; j++){
      A[k][j] /= pivot;
    }
    
    for(i=k+1; i<N; i++){
      del = A[i][k];
      for(j=k; j<N+1; j++){
	A[i][j] -= A[k][j]*del;
      }
    }
  }
  print(N,A);

  printf("backward elimination:\n");
  for(k=N-2; k>=0; k--){
      for(j=k+1; j<N+1; j++)
	  A[k][N] -= A[k][j]*A[j][N];
  }
  print(N,A);

  printf("coefficient:\n");
  for (i=0; i<N; i++)
    printf("      x%d = %5.2lf\n",i,A[i][N]);
}

int main(int argc, char *argv[]){
  FILE   *fp;
  char filename[256];
  int N,i,j;
  
  if(argc-1 !=2){
    printf("Error.\n");
    printf("Usage: ./a.out [Matrix size] [Filename]\n");
    exit(1);
  }
  
  N = atoi(argv[1]);
  strcpy(filename,argv[2]);
  
  double matrix[N][N+1];
  inputData(N,filename,matrix);
  printf("matrix:\n");
  print(N,matrix);
  
  ge(N,matrix);

  return 0;
}
