#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//ファイルから係数を入力
void  inputData(int N, char filename[256], double A[N][N+1])
{
  FILE *fp;
  int i,j;

  fp = fopen(filename,"r");
  if(fp==NULL){
    printf("Error.\n");
    printf("Cannot open file.\n");
    exit(1);
  }

  for(i=0; i<N; i++){
    for(j=0; j<N+1; j++){
      fscanf(fp, "%lf", &A[i][j]);
    }
  }
  
  fclose(fp);
}

//行列Aの要素を表示
void print(int N, double A[N][N+1]){

  int i,j;

  for (i=0; i<N; i++){
    for(j=0; j<N+1; j++){
      printf("%10.2lf",A[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//Gauss-Jordan elimination (ガウス・ジャルダン法)
void gj(int N, double A[N][N+1]){

  int i,j,k;
  double pivot,del;
  
  for(i=0; i<N; i++){
    pivot = A[i][i];
    for(j=i; j<N+1; j++)
      A[i][j] /= pivot;
  
    for(k=0; k<N; k++){
      del = A[k][i];
      for(j=i; j<N+1; j++)
	if( k != i)
	  A[k][j] -= del*A[i][j];
    }
  }

  printf("final matrix:\n");
  print(N,A);
  
  printf("coefficient:\n");
  for (i=0; i<N; i++)
    printf("      x%d = %5.2lf\n",i,A[i][N]);
}

int main(int argc, char *argv[]){
  FILE   *fp;
  char filename[256];
  int N,i,j;
  
  if(argc-1 !=2){
    printf("Error.\n");
    printf("Usage: ./a.out [Matrix size] [Filename]\n");
    exit(1);
  }
  
  N = atoi(argv[1]);
  strcpy(filename,argv[2]);
  
  double matrix[N][N+1];
  inputData(N,filename,matrix);
  printf("matrix:\n");
  print(N,matrix);

  gj(N,matrix);
  
  return 0;
}
