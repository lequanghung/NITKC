#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import sys

class GaussSeidel():
    def __init__(seft):
        seft.argv = sys.argv
        seft.argc = len(seft.argv)
        if (seft.argc != 4):
            print "Usage: python %s [datafile] [degree] [looptime]." % seft.argv[0]
            quit()    
        seft.filename = seft.argv[1]
        seft.N = int(seft.argv[2])
        seft.LOOP = int(seft.argv[3])
                
    def loop(seft):
        f =  open(seft.filename, 'r')
        line = 1
        mtx = []
        x = y = z =1
        while line:
            line = f.readline()
            # del '/n' and covert to float
            mtx.extend(np.array(line[:-1].split(), dtype= np.float32))  
        # reshaped to N*N+1 matrix
        mtx = np.array(mtx).reshape((seft.N,seft.N+1))

        for i in range(seft.LOOP):
            x = (mtx[0][3] - y*mtx[0][1] - z*mtx[0][2])/mtx[0][0]  
            y = (mtx[1][3] - x*mtx[1][0] - z*mtx[1][2])/mtx[1][1]
            z = (mtx[2][3] - x*mtx[2][0] - y*mtx[2][1])/mtx[2][2]
            print("[{0:>3}] {1:>10f} {2:>10f} {3:>10f}".format(i, x, y, z))     
    
if __name__ == '__main__':
    p = GaussSeidel().loop()
