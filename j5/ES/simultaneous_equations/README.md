<h1 style="text-align: center"> Gauss elimination method </h1>
## Step 1: forward elimination(前進消去)

<img src="http://latex.codecogs.com/gif.latex?\begin{align*}\begin{bmatrix}2.0&3.0&3.0&|&13.0\\1.0&3.0&2.0&|&13.0\\3.0&2.0&1.0&|&10.0\end{bmatrix}&\Rightarrow\begin{bmatrix}1.0&1.5&1.5&|&6.5\\&2.5&0.5&|&6.5\\&0.5&-3.5&|&-9.5&\end{bmatrix}\\\begin{bmatrix}1.0&1.5&1.5&|&6.5\\&1.0&0.2&|&2.6\\&&3.6&|&-10.8&\end{bmatrix}&\Rightarrow\begin{bmatrix}1.0&1.5&1.5&|&6.5\\&1.0&0.2&|&2.6\\&&1.0&|&3.0&\end{bmatrix}\end{align*}"/>
 
## Step 2: backward elimination(後退代入)
<img src="http://latex.codecogs.com/gif.latex?\begin{align*}&space;\begin{bmatrix}&space;1.0&1.5&1.5&|&6.5\\&space;&1.0&0.2&|&2.6\\&space;&&1.0&|&3.0&&space;\end{bmatrix}&space;\Rightarrow&space;\begin{bmatrix}&space;1.0&1.5&1.5&|&6.5\\&space;&1.0&&|&2.0\\&space;&&1.0&|&3.0&space;\end{bmatrix}&space;\Rightarrow&space;\begin{bmatrix}&space;1.0&&&|&1.0\\&space;&1.0&&|&2.0\\&space;&&1.0&|&3.0&space;\end{bmatrix}&space;\end{align*}"/>

<h1 style="text-align: center"> Gauss-Jordan elimination method </h1>

<h1 style="text-align: center"> Cramer's rule </h1>
<img p src="http://latex.codecogs.com/gif.latex?\begin{bmatrix}&space;2.0&3.0&8.0\\&space;3.0&-1.0&1.0\\&space;\end{bmatrix}"/>  より  
<img src="http://latex.codecogs.com/gif.latex?x&space;=&space;\frac{\begin{vmatrix}&space;8.0&3.0\\&space;1.0&-1.0&space;\end{vmatrix}}{\begin{vmatrix}&space;2.0&3.0\\&space;3.0&-1.0&space;\end{vmatrix}}&space;=&space;1.0,&space;y&space;=&space;\frac{\begin{vmatrix}&space;2.0&8.0\\&space;3.0&1.0&space;\end{vmatrix}}{\begin{vmatrix}&space;2.0&3.0\\&space;3.0&-1.0&space;\end{vmatrix}}&space;=&space;2.0"/>
