/*------------ Cramer's rule-------------------
　A'は行列Aの第j列をbで置き換えて得られる行列
　その時　x' = det(A')/det(A)

Ax+By=P    => [A B][x] -  P]
Cx+Dy=Q       [C D][y] -  Q]

=> x = det(PB QD) / det(AB CD)
=> y = det(AP CQ) / det(AB CD)
*/

#include <stdio.h>
#define N 2

//行列Aの要素を表示
void print(double A[N][N+1]){

  int i,j;

  for (i=0; i<N; i++){
    for(j=0; j<N+1; j++){
      printf("%5.2f",A[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//クラメルの公式
void cramer(double A[N][N+1]){

  double detA = A[0][0]*A[1][1]-A[1][0]*A[0][1];
  double detA1 = A[0][2]*A[1][1]-A[1][2]*A[0][1];
  double detA2 = A[0][0]*A[1][2]-A[1][0]*A[0][2];

  printf("x = %5.2f\n",detA1/detA);
  printf("y = %5.2f\n",detA2/detA);
}
int main(void){

  double A[N][N+1] = {{2,3,8},{-3,4,5}};
  print(A);
  cramer(A);
  return 0;
}
