#!/usr/bin/env python
# -*- coding:utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

# 通る点の座標
xp=[1, 2, 3, 4]
yp=[9, 6, 4, 3]

# 曲線の係数を求める行列
A=np.array([[ 1,  1,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0],  
            [ 8,  4,  2,  1,  0,  0,  0,  0,  0,  0,  0,  0],  
            [ 0,  0,  0,  0,  8,  4,  2,  1,  0,  0,  0,  0],  
            [ 0,  0,  0,  0, 27,  9,  3,  1,  0,  0,  0,  0],  
            [ 0,  0,  0,  0,  0,  0,  0,  0, 27,  9,  3,  1],  
            [ 0,  0,  0,  0,  0,  0,  0,  0, 64, 16,  4,  1],  
            [12,  4,  1,  0,-12, -4, -1,  0,  0,  0,  0,  0],
            [12,  2,  0,  0,-12, -2,  0,  0,  0,  0,  0,  0],  
            [ 0,  0,  0,  0, 27,  6,  1,  0,-27, -6, -1,  0],  
            [ 0,  0,  0,  0, 18,  2,  0,  0,-18, -2,  0,  0],  
            [ 6,  2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0],  
            [ 0,  0,  0,  0,  0,  0,  0,  0, 24,  2,  0,  0]])

b=np.array([9, 6, 6, 4, 4, 3, 0, 0, 0, 0, 0, 0])

# Ax=b より x= inver(A) x b
invA = np.linalg.inv(A)
x = np.dot(invA, b)
# 求めた係数を出力
print(x)

# グラフ描く
x1 = np.linspace(xp[0], xp[1], 20)
y1 = x[0]*x1*x1*x1+x[1]*x1*x1+x[2]*x1+x[3]
x2 = np.linspace(xp[1], xp[2], 20)
y2 = x[4]*x2*x2*x2+x[5]*x2*x2+x[6]*x2+x[7]
x3 = np.linspace(xp[2], xp[3], 20)
y3 = x[8]*x3*x3*x3+x[9]*x3*x3+x[10]*x3+x[11]

plt.plot(xp,yp,'ro')
plt.plot(x1, y1, 'b',label="S1")
plt.plot(x2, y2, 'g',label="S2")
plt.plot(x3, y3, 'y',label="S3")

plt.legend()
plt.grid(True)
plt.show()

