#!/usr/bin/env python
# -*- coding:utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

def f(x):
    return 1/(1+25*x*x)
    
def lagrange(x ,y):    
    print(x)
    print(y)
    N=np.linspace(-1,1, 100)
    xp=[]
    yp=[]

    for n in N:
        res = 0
        for i in range(len(x)):
            numer,denom = 1,1 # 分子,分母
            for j in range(len(x)):
                if(j!=i):
                    numer *= n-x[j]
                    denom *= x[i]-x[j]
            res += numer/denom*y[i]
        xp.append(n)
        yp.append(res)
                       
    plt.plot(N, f(N), 'g-', label='f(x)=1/(1+25*x*x)')
    plt.plot(x,y,'ro')
    plt.plot(xp, yp, 'b-', label='Lagrange polynomial')
    plt.legend()
    plt.grid(True)
    plt.show()

x1=np.linspace(-1, 1, 5)
x2=np.linspace(-1, 1, 11)
x3=np.linspace(-1, 1, 21)
y1=f(x1)
y2=f(x2)
y3=f(x3)

print("N=5:")
lagrange(x1,y1)
print("N=11:")
lagrange(x2,y2)
print("N=21:")
lagrange(x3,y3)
