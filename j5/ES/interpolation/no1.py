#!/usr/bin/env python
# -*- coding:utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

x=[1., 3., 4.]
y=[1., 2., 5.]
N=np.arange(-1, 5, 0.1)
xp=[]
yp=[]

for n in N:
    res = 0
    for i in range(len(x)):
        numer,denom = 1,1 # 分子,分母
        for j in range(len(x)):
            if(j!=i):
                numer *= n-x[j]
                denom *= x[i]-x[j]
        res += numer/denom*y[i]
    xp.append(n)
    yp.append(res)
   
plt.plot(x,y,'ro')
plt.plot(xp, yp, 'b',label="Lagrange polynomial")
plt.legend()
plt.grid(True)
plt.show()
