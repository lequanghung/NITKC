#include <stdio.h>
#include <math.h>
#define ESP 0.00001

double f_x(double x)
{
  return x*x - 2;
}

void root_of_2(double *a, double *b)
{
  double c = (*a+*b)/2;
  printf("%.30f\n",c);

  if( f_x(*a) * f_x(c) < 0 )
    {
      *b = c;
    }
  else
    {
      *a = c;
    }
}

int main(void)
{
  double a=1;
  double b=2;
  int i = 0;

  while(fabs(a-b)>ESP)
    {
      printf("%2d. ",i);
      root_of_2(&a,&b);
      i++;
    }

  return 0;
}
