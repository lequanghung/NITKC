#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define ESP 0.00001

double fx(double x){             //f(x) = x^2 -2
  return pow(x,2.0)-2.0;
}

double f1x(double x){           // f'(x) = 2x
  return 2.0*x;
}

int main (void){
  double a=2.0;
  double b=2.0;
  int i=0;

  do{
    i++;
    a = b;
    b = a - fx(a)/f1x(a);
    printf("%2d %.30lf\n",i,b);
  } while (fabs(a-b)>ESP);

  return 0;
}
