#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define ESP 0.00001  //

double fx(double x){    // caculate f(x) function
  return x*x-2;
}

void main(void){
  double a=0, b=2.0;
  double c=0;
  int i=0;

  while(fabs(a-b) > ESP){
    c=(a*fx(b)-fx(a)*b)/(fx(b)-fx(a));
    printf("%2d %.30lf\n",i,c);
    i++;
    if (fx(c)*fx(a) <= 0){          // roots in [a,w]
      b = c;
    } else {                            // roots in [w,b]
      a = c;
    }
  }
}
