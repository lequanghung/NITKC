#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "MT.h"

#define STEP 30
#define TIME 1e7
#define MAX 1e6

double fx (double x){
  return 1*pow(x, 4) + 2*x;
}

double TrapezoidalRule(double a, double b){
  double res = 0, y[STEP+1] = {0};
  double h = (b-a)/(double)STEP;
  int i;
  
  for (i =0; i<=STEP; i++){
    y[i] = a + h*i;
  }
  
  for (i = 1; i<STEP; i++){
    res += 2*(fx(y[i]));
  }
  res = h/2*(res+y[0]+y[STEP]);
  return res;
}

double MonteCarlo( double a, double b){

  double cnt=0, res = 0;
  int i;

  for (i = 0; i < TIME; i++){
    double x = a + genrand_real1() *(b-a);
    double y = genrand_real1() * MAX;

    if(y <= fx(x))
      cnt ++;
  }
  
  return MAX*(b-a)*cnt/TIME;
}

double Simpson( double a, double b){
 
  double res = 0, y[2*STEP+1] = {0};
  double h = (b-a)/(double)(2.0*STEP);
  int i;
  
  for (i =0; i<=2*STEP; i++){
    y[i] = a + h*i;
  }
  
  for (i = 1; i<2*STEP; i++){
    if ( i%2 == 1)
      res += 4*(fx(y[i]));
    else
      res += 2*(fx(y[i]));
  }
  res = h/3*(res+y[0]+y[2*STEP]);
  return res;
}

int main(int argc, char *argv[]){
  if ( argc != 3){
    printf ("Usage: %s [a] [b].\n", argv[0]);
  } else {
    printf ("台形: %.5f\n", TrapezoidalRule(atof(argv[1]), atof(argv[2])));
    printf ("シンプソン: %.5f\n", Simpson(atof(argv[1]), atof(argv[2])));
    printf ("モンテカルロ: %.5f\n", MonteCarlo(atof(argv[1]), atof(argv[2])));
  }
  
  return 0;
}
