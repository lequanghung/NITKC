#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "gauss_jordan.c"
#include "time.h"

#define LIMIT 54
#define MIN -2.5
#define MAX 10
//#define VIEW_DATA 1
//#define VIEW_LIST 1
//#define VIEW_MATRIX 1
#define COEF "coef.txt"
struct dataset {
  double x;
  double y;
};

void inputData(struct dataset data[], int dataNumber, char filename[256])
{
  FILE *fp;
  int i;

  fp = fopen(filename, "r");
  if(fp==NULL) {
    printf("Error.\n");
    printf("Can't open file.\n");
    exit(1);
  }

  for(i=0; i<dataNumber; i++) {
    fscanf(fp, "%lf", &data[i].x);
    fscanf(fp, "%lf", &data[i].y);
  }

  fclose(fp);
}

#ifdef VIEW_DATA
void dispData(struct dataset data[], int dataNumber)
{
  int i;

  for(i=0; i<dataNumber; i++) {
    printf("%.3e %.3e\n", data[i].x, data[i].y);
  }

}
#endif

double power(double x, int n)
{
  double i, sum=1;
  for(i=0; i<n; i++)
    {
      sum *= x;
    }
  return sum;
}

#ifdef VIEW_MATRIX
void show_matrix(int n, double matrix[n+1][n+2])
{
  int j,k;

   for(j=0; j<n+1; j++)
    {
      for(k=0; k<n+2; k++)
	{
	  printf("%8.3f ",matrix[j][k]);
	}
      printf("\n");
    }
}
#endif

#ifdef VIEW_LIST
void show_array(int n, double a[n+1])
{
  int k;
   for(k=0; k<n+1; k++)
    {
      printf("%8.3e ",a[k]);
    }
  puts("");
}
#endif

void plot_file(int n, char filename[256])
{
  FILE *gp,*fp;
  char name[5];
  char coef_str[100000] = {'\0'};

  snprintf(name,5,"%d",n);
  fp = fopen(COEF,"r");
  fgets(coef_str,100000,fp);
  //printf("%s\n",coef_str);
  fclose(fp);

  gp = popen("gnuplot -persist","w");
  fprintf(gp,"set terminal png\n");
  fprintf(gp,"set output '%s.png'\n",name);
  // fprintf(gp,"set xrange [-1:15]\n",MIN,MAX);
  fprintf(gp,"plot \"%s\", %s \n",filename,coef_str);
  fprintf(gp,"exit()\n");
  pclose(gp);
}

int lsm(struct dataset data[], int dataNumber,char filename[256])
{

#ifdef VIEW_DATA
  dispData(data, dataNumber);
  printf("\n");
#endif
  /* $B$3$3$+$i:G>.Fs>hK!$N%W%m%0%i%`$r=q$/(B */
  int j,m,n,n_min=1,k,temp = 0;
  double sum, error = 0, error_min = 0;
  // printf("input n = ");
  // scanf("%d",&n);
  for(n=1; n<LIMIT+1; n++){

    double matrix[n+1][n+2];
    double list_0[2*n+1];   // x^2n -> x^2,x,1
    double list_1[n+1];   // x^n*y -> x^2y,xy,y

    //make list_0
    temp = 0;
    for(j=0; j<2*n+1; j++)
      {
	list_0[j] = 0;
	for(m=0; m<dataNumber; m++)
	  {
	    list_0[j] += power(data[m].x,2*n-temp);
	  }
	temp++;
      }

    //make list_2
    temp = 0;
    for(j=0; j<n+1; j++)
      {
	list_1[j] = 0;
	for(m=0; m<dataNumber; m++)
	  {
	    list_1[j] += power(data[m].x,n-temp)*data[m].y;
	  }
	temp++;
      }
#ifdef VIEW_LIST
    printf("list of %d level :\n",n);
    show_array(2*n,list_0);
    show_array(n,list_1);
#endif

    //copy list to matrix
    temp = 0;
    for(j=0; j<n+1; j++)
      {
	for(k=0; k<n+1; k++)
	  {
	    matrix[j][k] = list_0[k+temp];
	  }
	temp++;
	matrix[j][n+1] = list_1[j];
      }

#ifdef VIEW_MATRIX
    printf("\nmatrix of %d level:\n",n);
    show_matrix(n,matrix);
#endif

    solve(n+1,matrix);
    //show_matrix(n,matrix);

    //caculate error
    error = 0;
    for(m=0; m<dataNumber; m++)
      {
	sum = 0;
	temp = 0;
	for(j=0; j<n+1; j++)
	  {
	    sum += matrix[j][n+1]*power(data[m].x,n-temp);
	    temp++;
	  }
	error += 0.5*power(data[m].y - sum,2);
	//	printf("error = %lf\n",error);
      }
    printf("error of %d level = %.5e\n\n",n,error);
    if(error < error_min)
      {
	error_min = error;
	n_min = n;
      }
    else if(n==1)
      {
	error_min = error;
      }
    plot_file(n,filename);
  }
  printf(" error min = %.5e when n = %d\n",error_min,n_min);
  return n_min;
}

int main(int argc, char *argv[])
{
  struct dataset *data;
  int dataNumber,min;
  char filename[256];

  if(argc-1 != 2) {
    printf("Error.\n");
    printf("Usage: ./a.out [Data Number] [Filename]\n");
    exit(1);
  }
  //  srand(time(NULL));
  dataNumber = atoi(argv[1]);
  strcpy(filename, argv[2]);

  data = (struct dataset *)malloc(sizeof(struct dataset)*dataNumber);

  inputData(data, dataNumber, filename);
  min = lsm(data, dataNumber,filename);
  //plot_file(min,filename);
  return 0;
}
