#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define EPS 1e-20
#define FILENAME "coef.txt"

int solve(int N, double a[N][N+1])
{
  double pivot, del;
  int i, j, k, l;

  for (i=0; i<N; i++) {
    pivot = a[i][i];
    if (fabs(pivot) < EPS) {
      printf("ピボットが許容誤差以下\n");
      return 1;
    }
    for (j=i; j<N+1; j++) {
      a[i][j] = a[i][j]/pivot;
    }

    for (k=0; k<N; k++) {
      del = a[k][i];
      for (j=i; j<N+1; j++) {
	if (k != i) {
	  a[k][j] -= del*a[i][j];
	}
      }
    }

  }

  FILE *fp;
  fp = fopen(FILENAME,"w");
  printf("coefficient :\n");
  for (l=0; l<N; l++) {
    //a[l][N] = l+1;   //係数を1,2,3...にする
    printf("X%d = %.5e\n", l, a[l][N]);
    fprintf(fp,"+(%.5e)*x**%d",a[l][N],N-1-l);
  }
  fprintf(fp,"\n");
  fclose(fp);
  return 0;
}
