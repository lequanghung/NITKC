#include <stdlib.h>
#include <curses.h>
#include <time.h>
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define L 60
#define H 20
//#define DEBUG
#define TRUE 1
#define FALSE 0
#define LEVELUP -1
extern char getChar(void);

typedef struct data{
  int x;
  int y;
  int crash;
  struct data *next;
} DATA;

void init_screen();
void init_data(int lv, DATA *post_data);
void dis_field(DATA *post_data);
int robots_crash(DATA *post_data);
void mv_robots(int x, int y, DATA *post_data);
int mv_player(int x, int y, DATA *post_data);
int check_gm(DATA *post_data);
int main(void);

void init_screen(){
  /* init screen before use */
  initscr();
  cbreak();   /* wait for keyboard input */
  nonl();
  scrollok(stdscr,TRUE);   /* enable scrolling window */
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);
}

void init_data(int lv, DATA *post_data){
  /* init player and robots info */

  int i = 0;
  DATA *p,*new;

  post_data->x = random()%L + 1;  /* random player postition */
  post_data->y = random()%H + 1;
  post_data->crash = 0;
  p = post_data;

  /* random 5*lv robots post */
  for ( i = 0; i < MIN(5*lv,40); i++){
    new = (DATA *)malloc(sizeof(DATA));
    new->x = random()%L + 1;
    new->y = random()%H + 1;
    new->crash = 0;
    if(new->x == post_data->x && new->y == post_data->y) i--;
    else{
      p -> next = new;
      p = new;
    }
  }
  p -> next = NULL;
}

void dis_field(DATA *post_data){
  /* display game field */
  int i,j;

  for( i = 0; i <= L+1; i++){  /* display field border */
    mvprintw(0,i,"%c",'/');
    mvprintw(H+1,i,"%c",'/');
  }
  for( j = 0; j <= H+1; j++){
    mvprintw(j,0,"%c",'/');
    mvprintw(j,L+1,"%c",'/');
  }

  /* display player and robots */
  DATA *p;
  p = post_data;
  
#ifdef DEBUG
  i = 0;
#endif
  
  mvprintw(p->y,p->x,"%c",'@');
  for(p = p->next; p != NULL ; p = p->next){
    if( p->crash == TRUE){
      mvprintw(p->y,p->x,"%c",'*');
    }
    else mvprintw(p->y,p->x,"%c",'+');

#ifdef DEBUG
    mvprintw(21,L+3," player: %d %d",post_data->x,post_data->y);
    mvprintw(i,L+3,"%d robot %d: %d  %d ",p->crash,i,p->x,p->y);
    i++;
#endif

  }
}

int robots_crash(DATA *post_data){
  /* return number of new robot crashed */
  DATA *p,*q;
  int cnt = 0;
  
  for(q = post_data->next; q->next != NULL; q=q->next){
    for(p = q->next;p != NULL;p=p->next){
      if(q->x == p->x && q->y == p->y){
	if(p->crash != 1){
	  p->crash = 1; cnt++;
	}
	if(q->crash != 1){
	  q->crash = 1; cnt++;
	}
      }
    }
  }
  return cnt;
}

void mv_robots(int x, int y, DATA *post_data){
  /*  move robots post  */
  DATA *p;
  
  for( p = post_data->next; p != NULL;p = p->next){
    if(p->crash == FALSE){
      mvprintw(p->y,p->x,"%c",' ');
      if(post_data->x > p->x) p->x ++;
      else if(post_data->x < p->x) p->x --;
      if(post_data->y > p->y) p->y ++;
      else if(post_data->y < p->y) p->y --;
    }
  }
}

int mv_player(int x, int y, DATA *post_data){
  
  DATA *p = post_data->next;
  int temp_x,temp_y,tlp = 0;
  
  /* player teleport */
  if(x == 5 && y == 5){
    temp_x = random()%L + 1;
    temp_y = random()%H + 1;
    tlp = 1;
  }
  else { /* normal move */
    temp_x = post_data->x +x;
    temp_y = post_data->y +y;
  }
  
  /* check new post condition
     in game field,
     has robot (crashed robot) or not */
  if(temp_x > 0 && temp_x < L+1
     && temp_y > 0 && temp_y < H+1){
    while( p != NULL){
      if(temp_x == p->x && temp_y == p->y){
	if(tlp == TRUE){
	  temp_x = random()%L + 1;
	  temp_y = random()%H + 1;
	  p = post_data->next;
	}
	else return 0;
      }
      p = p->next;
    }
    mvprintw(post_data->y,post_data->x,"%c",' ');
    post_data->x = temp_x;
    post_data->y = temp_y;
    return TRUE;
  }
}

int check_gm(DATA *post_data){
  /* return 1 when gameover,
     return 0 to continue,
     return -1 to level up */
  DATA *p;
  int cnt = 0;
  
  for(p = post_data->next; p != NULL; p=p->next){
    if(post_data->x == p->x && post_data->y == p->y){
      return TRUE;
    }
    if(p->crash == FALSE){
      cnt = TRUE;
    }
  }
  if( cnt == TRUE) return FALSE;
  else return LEVELUP;
}

int main(void){
  
  int x,y,lv = 1,score = 0;
  DATA post_data;
  int tmp = 0;

  /* init for 1st time run */
  srandom(time(NULL));
  init_screen();
  init_data(lv,&post_data);

  /* loop until gameover */
  while(check_gm(&post_data) == FALSE){
    x = 0;y = 0;
    dis_field(&post_data);
    /* display game level and score */
    mvprintw(H+2,3," Level:%d Score:%d    ",lv,score);
    move(H+2,22);
    switch(getch()){
    case '1': x--; y++; break;
    case '2': y++; break;
    case '3': x++; y++; break;
    case '4': x--; break;
    case '5': break;
    case '6': x++; break;
    case '7': x--; y--; break;
    case '8': y--;break;
    case '9': x++; y--;break;
    case '0': x = 5; y = 5; break;
    default : x = -5;break;
    }
    if(x > -2){ /* not get other input */
      if(mv_player(x,y,&post_data) == TRUE){
	mv_robots(x,y,&post_data);
	score += robots_crash(&post_data);
      }
      if(check_gm(&post_data) == -1){ /* lever up */
	score += lv*10;
	clear();
	lv++;
	init_data(lv,&post_data);
      }
      refresh();
    }
  }
  dis_field(&post_data);
  mvprintw(H+3,3,"%s","Game over !!!");
  getch();
  endwin();
  return 0;
}
